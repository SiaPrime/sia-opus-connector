import api from '../api'

export default (apiOptions, actions) => {
  const {
    updateNotifications,
    getResource,
    getNotifications,
    getSortState // eslint-disable-line no-unused-vars
  } = actions
  return {
    id: 'sort',
    shouldBeAvailable: () => true,
    handler: async ({ sortBy, sortDirection }) => {
      const id = getResource().id
      return api.getChildrenForId(apiOptions, { id, sortBy, sortDirection })
    }
  }
}
